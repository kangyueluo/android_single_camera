package com.koin.koincamera.camera;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    Camera camera;

    public CameraSurfaceView(Context context) {
        super(context);

        SurfaceHolder holder = this.getHolder();
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            // Open the Camera in preview mode
            this.camera = Camera.open(0);
            this.camera.setPreviewDisplay(holder);
        } catch (IOException ioe) {
            ioe.printStackTrace(System.out);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // The default orientation is landscape, so for a portrait app like this
        // one we need to rotate the view 90 degrees.
        camera.setDisplayOrientation(90);

        // IMPORTANT: We must call startPreview() on the camera before we take
        // any pictures
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // Surface will be destroyed when replaced with a new screen
        // Always make sure to release the Camera instance
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}
